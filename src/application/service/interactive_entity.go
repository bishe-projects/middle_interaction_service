package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_entity/entity"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_entity/service"
)

var InteractiveEntityApp = new(InteractiveEntity)

type InteractiveEntity struct{}

func (a *InteractiveEntity) Create(entity *entity.InteractiveEntity) *business_error.BusinessError {
	return service.InteractiveEntityDomain.Create(entity)
}

func (a *InteractiveEntity) All(bizId *int64) ([]*entity.InteractiveEntity, *business_error.BusinessError) {
	return service.InteractiveEntityDomain.All(bizId)
}

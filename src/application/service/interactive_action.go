package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_action/entity"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_action/service"
)

var InteractiveActionApp = new(InteractiveAction)

type InteractiveAction struct{}

func (a *InteractiveAction) Create(action *entity.InteractiveAction) *business_error.BusinessError {
	return service.InteractiveActionDomain.Create(action)
}

func (a *InteractiveAction) All(bizId *int64) ([]*entity.InteractiveAction, *business_error.BusinessError) {
	return service.InteractiveActionDomain.All(bizId)
}

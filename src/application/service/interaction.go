package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interaction/entity"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interaction/service"
)

var InteractionApp = new(Interaction)

type Interaction struct{}

func (a *Interaction) Create(ctx context.Context, interaction *entity.Interaction) *business_error.BusinessError {
	return service.InteractionDomain.Create(ctx, interaction)
}

func (a *Interaction) Del(ctx context.Context, interaction *entity.Interaction) *business_error.BusinessError {
	return service.InteractionDomain.Del(ctx, interaction)
}

func (a *Interaction) GetList(ctx context.Context, bizId, actionId, initiatorEntityId, acceptorEntityId int64, initiatorId, acceptorId *int64) ([]*entity.Interaction, *business_error.BusinessError) {
	return service.InteractionDomain.GetList(ctx, bizId, actionId, initiatorEntityId, acceptorEntityId, initiatorId, acceptorId)
}

package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

type InteractiveAction struct {
	ID    int64
	BizId int64
	Name  string
	Desc  string
}

func (i *InteractiveAction) Create() *business_error.BusinessError {
	po := i.ConvertToPO()
	err := repo.InteractiveActionRepo.InteractiveActionDao.Create(po)
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[InteractiveActionAggregate] create interactive action failed, err=%s", err)
		return biz_error.CreateInteractiveActionErr
	}
	i.FillFromPO(po)
	return nil
}

type InteractiveActionList []*InteractiveAction

func (i *InteractiveActionList) All(bizId *int64) *business_error.BusinessError {
	poList, err := repo.InteractiveActionRepo.InteractiveActionDao.All(bizId)
	if err != nil {
		klog.Errorf("[InteractiveActionAggregate] get all interactive action list failed, err=%s", err)
		return biz_error.AllInteractiveActionListErr
	}
	i.FillFromPOList(poList)
	return nil
}

// converter
func (i *InteractiveAction) ConvertToPO() *po.InteractiveAction {
	return &po.InteractiveAction{
		ID:    i.ID,
		BizId: i.BizId,
		Name:  i.Name,
		Desc:  i.Desc,
	}
}

func (i *InteractiveAction) FillFromPO(po *po.InteractiveAction) {
	i.ID = po.ID
	i.BizId = po.BizId
	i.Name = po.Name
	i.Desc = po.Desc
}

func (i *InteractiveActionList) FillFromPOList(poList []*po.InteractiveAction) {
	*i = make([]*InteractiveAction, 0, len(poList))
	for _, po := range poList {
		action := new(InteractiveAction)
		action.FillFromPO(po)
		*i = append(*i, action)
	}
}

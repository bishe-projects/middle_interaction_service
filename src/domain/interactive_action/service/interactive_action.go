package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_action/entity"
)

var InteractiveActionDomain = new(InteractiveAction)

type InteractiveAction struct{}

func (d *InteractiveAction) Create(action *entity.InteractiveAction) *business_error.BusinessError {
	return action.Create()
}

func (d *InteractiveAction) All(bizId *int64) ([]*entity.InteractiveAction, *business_error.BusinessError) {
	actionList := new(entity.InteractiveActionList)
	err := actionList.All(bizId)
	return *actionList, err
}

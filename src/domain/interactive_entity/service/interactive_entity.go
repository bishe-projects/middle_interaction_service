package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_entity/entity"
)

var InteractiveEntityDomain = new(InteractiveEntity)

type InteractiveEntity struct{}

func (d *InteractiveEntity) Create(entity *entity.InteractiveEntity) *business_error.BusinessError {
	return entity.Create()
}

func (d *InteractiveEntity) All(bizId *int64) ([]*entity.InteractiveEntity, *business_error.BusinessError) {
	entityList := new(entity.InteractiveEntityList)
	err := entityList.All(bizId)
	return *entityList, err
}

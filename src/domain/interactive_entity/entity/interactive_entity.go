package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

type InteractiveEntity struct {
	ID    int64
	BizId int64
	Name  string
	Desc  string
}

func (i *InteractiveEntity) Create() *business_error.BusinessError {
	po := i.ConvertToPO()
	err := repo.InteractiveEntityRepo.InteractiveEntityDao.Create(po)
	if err == error_enum.ErrDuplicateEntry {
		return business_error.DataExistsErr
	}
	if err != nil {
		klog.Errorf("[InteractiveEntityAggregate] create interactive entity failed, err=%s", err)
		return biz_error.CreateInteractiveEntityErr
	}
	i.FillFromPO(po)
	return nil
}

type InteractiveEntityList []*InteractiveEntity

func (i *InteractiveEntityList) All(bizId *int64) *business_error.BusinessError {
	poList, err := repo.InteractiveEntityRepo.InteractiveEntityDao.All(bizId)
	if err != nil {
		klog.Errorf("[InteractiveEntityAggregate] get all interactive entity failed, err=%s", err)
		return biz_error.AllInteractiveEntityListErr
	}
	i.FillFromPOList(poList)
	return nil
}

// converter
func (i *InteractiveEntity) ConvertToPO() *po.InteractiveEntity {
	return &po.InteractiveEntity{
		ID:    i.ID,
		BizId: i.BizId,
		Name:  i.Name,
		Desc:  i.Desc,
	}
}

func (i *InteractiveEntity) FillFromPO(po *po.InteractiveEntity) {
	i.ID = po.ID
	i.BizId = po.BizId
	i.Name = po.Name
	i.Desc = po.Desc
}

func (i *InteractiveEntityList) FillFromPOList(poList []*po.InteractiveEntity) {
	*i = make([]*InteractiveEntity, 0, len(poList))
	for _, po := range poList {
		entity := new(InteractiveEntity)
		entity.FillFromPO(po)
		*i = append(*i, entity)
	}
}

package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interaction/entity"
)

var InteractionDomain = new(Interaction)

type Interaction struct{}

func (d *Interaction) Create(ctx context.Context, interaction *entity.Interaction) *business_error.BusinessError {
	return interaction.Create(ctx)
}

func (d *Interaction) Del(ctx context.Context, interaction *entity.Interaction) *business_error.BusinessError {
	return interaction.Del(ctx)
}

func (d *Interaction) GetList(ctx context.Context, bizId, actionId, initiatorEntityId, acceptorEntityId int64, initiatorId, acceptorId *int64) ([]*entity.Interaction, *business_error.BusinessError) {
	interactionList := new(entity.InteractionList)
	err := interactionList.GetList(ctx, bizId, actionId, initiatorEntityId, acceptorEntityId, initiatorId, acceptorId)
	return *interactionList, err
}

package entity

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

type Interaction struct {
	BizId             int64
	ActionId          int64
	InitiatorEntityId int64
	InitiatorId       int64
	AcceptorEntityId  int64
	AcceptorId        int64
}

func (i *Interaction) Create(ctx context.Context) *business_error.BusinessError {
	po := i.ConvertToPO()
	err := repo.InteractionRepo.InteractionDao.Create(ctx, po)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractionAggregate] create interaction failed, err=%s", err)
		return biz_error.CreateInteractionErr
	}
	return nil
}

func (i *Interaction) Del(ctx context.Context) *business_error.BusinessError {
	po := i.ConvertToPO()
	err := repo.InteractionRepo.InteractionDao.Del(ctx, po)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractionAggregate] delete interaction failed, err=%s", err)
		return biz_error.DeleteInteractionErr
	}
	return nil
}

type InteractionList []*Interaction

func (i *InteractionList) GetList(ctx context.Context, bizId, actionId, initiatorEntityId, acceptorEntityId int64, initiatorId, acceptorId *int64) *business_error.BusinessError {
	poList, err := repo.InteractionRepo.InteractionDao.GetList(ctx, bizId, actionId, initiatorEntityId, acceptorEntityId, initiatorId, acceptorId)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractionAggregate] get interaction list failed, err=%s", err)
		return biz_error.GetInteractionListErr
	}
	i.FillFromPOList(poList)
	return nil
}

// converter
func (i *Interaction) ConvertToPO() *po.Interaction {
	return &po.Interaction{
		BizId:             i.BizId,
		ActionId:          i.ActionId,
		InitiatorEntityId: i.InitiatorEntityId,
		InitiatorId:       i.InitiatorId,
		AcceptorEntityId:  i.AcceptorEntityId,
		AcceptorId:        i.AcceptorId,
	}
}

func (i *Interaction) FillFromPO(po *po.Interaction) {
	i.BizId = po.BizId
	i.ActionId = po.ActionId
	i.InitiatorEntityId = po.InitiatorEntityId
	i.InitiatorId = po.InitiatorId
	i.AcceptorEntityId = po.AcceptorEntityId
	i.AcceptorId = po.AcceptorId
}

func (i *InteractionList) FillFromPOList(poList []*po.Interaction) {
	*i = make([]*Interaction, 0, len(poList))
	for _, po := range poList {
		interaction := new(Interaction)
		interaction.FillFromPO(po)
		*i = append(*i, interaction)
	}
}

package repo

import (
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

type InteractiveEntityDao interface {
	Create(entity *po.InteractiveEntity) error
	All(bizId *int64) ([]*po.InteractiveEntity, error)
}

var InteractiveEntityRepo = NewInteractiveEntity(mysql.InteractiveEntityDao)

type InteractiveEntity struct {
	InteractiveEntityDao InteractiveEntityDao
}

func NewInteractiveEntity(interactiveDao InteractiveEntityDao) *InteractiveEntity {
	return &InteractiveEntity{
		InteractiveEntityDao: interactiveDao,
	}
}

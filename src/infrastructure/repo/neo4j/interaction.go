package neo4j

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j/dbtype"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

var InteractionDao = new(Interaction)

type Interaction struct{}

const DatabaseName = "neo4j"

func (r *Interaction) Create(ctx context.Context, interaction *po.Interaction) error {
	session := driver.NewSession(ctx, neo4j.SessionConfig{DatabaseName: DatabaseName})
	defer session.Close(ctx)
	// WriteTransaction retries the operation in case of transient errors by
	// invoking the anonymous function multiple times until it succeeds.
	_, err := session.ExecuteWrite(ctx,
		func(tx neo4j.ManagedTransaction) (any, error) {
			// To learn more about the Cypher syntax, see https://neo4j.com/docs/cypher-manual/current/
			// The Reference Card is also a good resource for keywords https://neo4j.com/docs/cypher-refcard/current/
			query := `
				MERGE (initiator:Entity{entity_id:$initiator_entity_id, id:$initiator_id})
				MERGE (acceptor:Entity{entity_id:$acceptor_entity_id, id:$acceptor_id})
				MERGE (initiator)-[:ACTION{biz_id: $biz_id, id: $action_id}]->(acceptor)
				RETURN initiator, acceptor`
			params := map[string]any{
				"biz_id":              interaction.BizId,
				"action_id":           interaction.ActionId,
				"initiator_entity_id": interaction.InitiatorEntityId,
				"acceptor_entity_id":  interaction.AcceptorEntityId,
				"initiator_id":        interaction.InitiatorId,
				"acceptor_id":         interaction.AcceptorId,
			}
			result, err := tx.Run(ctx, query, params)
			if err != nil {
				// Return the error received from driver here to indicate rollback,
				// the error is analyzed by the driver to determine if it should try again.
				return nil, err
			}
			// Collects all records and commits the transaction (as long as
			// Collect doesn't return an error).
			// Beware that Collect will buffer the records in memory.
			return result.Collect(ctx)
		})
	return err
}

func (r *Interaction) Del(ctx context.Context, interaction *po.Interaction) error {
	session := driver.NewSession(ctx, neo4j.SessionConfig{DatabaseName: DatabaseName})
	defer session.Close(ctx)
	// WriteTransaction retries the operation in case of transient errors by
	// invoking the anonymous function multiple times until it succeeds.
	_, err := session.ExecuteWrite(ctx,
		func(tx neo4j.ManagedTransaction) (any, error) {
			// To learn more about the Cypher syntax, see https://neo4j.com/docs/cypher-manual/current/
			// The Reference Card is also a good resource for keywords https://neo4j.com/docs/cypher-refcard/current/
			query := `
				MATCH (initiator:Entity{entity_id:$initiator_entity_id, id:$initiator_id})-[action:ACTION{biz_id: $biz_id, id: $action_id}]->(acceptor:Entity{entity_id:$acceptor_entity_id, id:$acceptor_id})
				DELETE action`
			params := map[string]any{
				"biz_id":              interaction.BizId,
				"action_id":           interaction.ActionId,
				"initiator_entity_id": interaction.InitiatorEntityId,
				"acceptor_entity_id":  interaction.AcceptorEntityId,
				"initiator_id":        interaction.InitiatorId,
				"acceptor_id":         interaction.AcceptorId,
			}
			result, err := tx.Run(ctx, query, params)
			if err != nil {
				// Return the error received from driver here to indicate rollback,
				// the error is analyzed by the driver to determine if it should try again.
				return nil, err
			}
			// Collects all records and commits the transaction (as long as
			// Collect doesn't return an error).
			// Beware that Collect will buffer the records in memory.
			return result.Collect(ctx)
		})
	return err
}

func (r *Interaction) GetList(ctx context.Context, bizId, actionId, initiatorEntityId, acceptorEntityId int64, initiatorId, acceptorId *int64) ([]*po.Interaction, error) {
	session := driver.NewSession(ctx, neo4j.SessionConfig{DatabaseName: DatabaseName})
	defer session.Close(ctx)
	result, err := session.ExecuteRead(ctx,
		func(tx neo4j.ManagedTransaction) (any, error) {
			// To learn more about the Cypher syntax, see https://neo4j.com/docs/cypher-manual/current/
			// The Reference Card is also a good resource for keywords https://neo4j.com/docs/cypher-refcard/current/
			query := `
				MATCH (initiator:Entity)-[action:ACTION]->(acceptor:Entity)
				WHERE initiator.entity_id = $initiator_entity_id AND acceptor.entity_id = $acceptor_entity_id AND action.id = $action_id AND action.biz_id = $biz_id
			`
			params := map[string]any{
				"biz_id":              bizId,
				"action_id":           actionId,
				"initiator_entity_id": initiatorEntityId,
				"acceptor_entity_id":  acceptorEntityId,
			}
			if initiatorId != nil {
				query += " AND initiator.id = $initiator_id"
				params["initiator_id"] = initiatorId
			}
			if acceptorId != nil {
				query += " AND acceptor.id = $acceptor_id"
				params["acceptor_id"] = acceptorId
			}
			query += " RETURN initiator, acceptor"
			result, err := tx.Run(ctx, query, params)
			if err != nil {
				// Return the error received from driver here to indicate rollback,
				// the error is analyzed by the driver to determine if it should try again.
				return nil, err
			}
			// Collects all records and commits the transaction (as long as
			// Collect doesn't return an error).
			// Beware that Collect will buffer the records in memory.
			return result.Collect(ctx)
		})
	if err != nil {
		return nil, err
	}
	records := result.([]*neo4j.Record)
	interactions := make([]*po.Interaction, 0)
	for _, record := range records {
		initiator := record.Values[0].(dbtype.Node)
		acceptor := record.Values[1].(dbtype.Node)
		interactions = append(interactions, &po.Interaction{
			BizId:             bizId,
			ActionId:          actionId,
			InitiatorEntityId: initiator.Props["entity_id"].(int64),
			InitiatorId:       initiator.Props["id"].(int64),
			AcceptorEntityId:  acceptor.Props["entity_id"].(int64),
			AcceptorId:        acceptor.Props["id"].(int64),
		})
	}
	klog.CtxInfof(ctx, "interactions=%+v", interactions)
	return interactions, nil
}

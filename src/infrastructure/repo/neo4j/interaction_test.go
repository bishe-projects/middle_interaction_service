package neo4j

import (
	"context"
	"github.com/apache/thrift/lib/go/thrift"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
	"testing"
)

func TestInteraction_GetList(t *testing.T) {
	type args struct {
		ctx               context.Context
		bizId             int64
		actionId          int64
		initiatorEntityId int64
		acceptorEntityId  int64
		initiatorId       *int64
		acceptorId        *int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				ctx:               context.Background(),
				bizId:             1,
				actionId:          1,
				initiatorEntityId: 1,
				initiatorId:       thrift.Int64Ptr(1),
				acceptorEntityId:  2,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Interaction{}
			interactions, err := r.GetList(tt.args.ctx, tt.args.bizId, tt.args.actionId, tt.args.initiatorEntityId, tt.args.acceptorEntityId, tt.args.initiatorId, tt.args.acceptorId)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for _, interaction := range interactions {
				t.Logf("interaction=%+v", interaction)
			}
		})
	}
}

func TestInteraction_Create(t *testing.T) {
	type args struct {
		ctx         context.Context
		interaction *po.Interaction
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				ctx: context.Background(),
				interaction: &po.Interaction{
					BizId:             1,
					ActionId:          1,
					InitiatorEntityId: 1,
					InitiatorId:       1,
					AcceptorEntityId:  2,
					AcceptorId:        2,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Interaction{}
			if err := r.Create(tt.args.ctx, tt.args.interaction); (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestInteraction_Del(t *testing.T) {
	type args struct {
		ctx         context.Context
		interaction *po.Interaction
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				ctx: context.Background(),
				interaction: &po.Interaction{
					BizId:             1,
					ActionId:          1,
					InitiatorEntityId: 1,
					InitiatorId:       1,
					AcceptorEntityId:  2,
					AcceptorId:        2,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Interaction{}
			if err := r.Del(tt.args.ctx, tt.args.interaction); (err != nil) != tt.wantErr {
				t.Errorf("Del() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

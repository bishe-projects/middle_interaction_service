package neo4j

import "github.com/neo4j/neo4j-go-driver/v5/neo4j"

var (
	driver neo4j.DriverWithContext
	err    error
)

func init() {
	uri := "neo4j+s://b6b34bef.databases.neo4j.io"
	auth := neo4j.BasicAuth("neo4j", "gk2QWqgj9o2LZEKDYC44wl76wXyniiHTopX7d8g8iMo", "")
	// You typically have one driver instance for the entire application. The
	// driver maintains a pool of database connections to be used by the sessions.
	// The driver is thread safe.
	driver, err = neo4j.NewDriverWithContext(uri, auth)
	if err != nil {
		panic(err)
	}
}

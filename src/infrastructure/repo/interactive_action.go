package repo

import (
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

type InteractiveActionDao interface {
	Create(action *po.InteractiveAction) error
	All(bizId *int64) ([]*po.InteractiveAction, error)
}

var InteractiveActionRepo = NewInteractiveAction(mysql.InteractiveActionDao)

type InteractiveAction struct {
	InteractiveActionDao InteractiveActionDao
}

func NewInteractiveAction(interactiveActionDao InteractiveActionDao) *InteractiveAction {
	return &InteractiveAction{
		InteractiveActionDao: interactiveActionDao,
	}
}

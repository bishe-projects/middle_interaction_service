package repo

import (
	"context"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/neo4j"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

type InteractionDao interface {
	Create(ctx context.Context, interaction *po.Interaction) error
	Del(ctx context.Context, interaction *po.Interaction) error
	GetList(ctx context.Context, bizId, actionId, initiatorEntityId, acceptorEntityId int64, initiatorId, acceptorId *int64) ([]*po.Interaction, error)
}

var InteractionRepo = NewInteraction(neo4j.InteractionDao)

type Interaction struct {
	InteractionDao InteractionDao
}

func NewInteraction(interactionDao InteractionDao) *Interaction {
	return &Interaction{
		InteractionDao: interactionDao,
	}
}

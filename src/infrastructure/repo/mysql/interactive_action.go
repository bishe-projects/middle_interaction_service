package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

var InteractiveActionDao = new(InteractiveAction)

type InteractiveAction struct{}

const InteractiveActionTable = "interaction_action"

func (r *InteractiveAction) Create(action *po.InteractiveAction) error {
	err := db.Table(InteractiveActionTable).Create(&action).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *InteractiveAction) All(bizId *int64) ([]*po.InteractiveAction, error) {
	var actionList []*po.InteractiveAction
	d := db.Table(InteractiveActionTable)
	if bizId != nil {
		d = d.Where("biz_id = ?", *bizId)
	}
	result := d.Scan(&actionList)
	return actionList, result.Error
}

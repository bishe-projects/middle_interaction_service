package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/middle_interaction_service/src/infrastructure/repo/po"
)

var InteractiveEntityDao = new(InteractiveEntity)

type InteractiveEntity struct{}

const InteractiveEntityTable = "interaction_entity"

func (r *InteractiveEntity) Create(entity *po.InteractiveEntity) error {
	err := db.Table(InteractiveEntityTable).Create(&entity).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *InteractiveEntity) All(bizId *int64) ([]*po.InteractiveEntity, error) {
	var entityList []*po.InteractiveEntity
	d := db.Table(InteractiveEntityTable)
	if bizId != nil {
		d = d.Where("biz_id = ?", *bizId)
	}
	result := d.Scan(&entityList)
	return entityList, result.Error
}

package po

type InteractiveAction struct {
	ID    int64
	BizId int64
	Name  string
	Desc  string
}

package po

type Interaction struct {
	BizId             int64
	ActionId          int64
	InitiatorEntityId int64
	InitiatorId       int64
	AcceptorEntityId  int64
	AcceptorId        int64
}

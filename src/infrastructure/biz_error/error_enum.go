package biz_error

import "gitlab.com/bishe-projects/common_utils/business_error"

var (
	CreateInteractiveEntityErr  = business_error.NewBusinessError("create interactive entity failed", -10001)
	AllInteractiveEntityListErr = business_error.NewBusinessError("get all interactive entity list failed", -10002)
	CreateInteractiveActionErr  = business_error.NewBusinessError("create interactive action failed", -10003)
	AllInteractiveActionListErr = business_error.NewBusinessError("get all interactive action list failed", -10004)
	CreateInteractionErr        = business_error.NewBusinessError("create interaction failed", -10005)
	DeleteInteractionErr        = business_error.NewBusinessError("delete interaction failed", -10006)
	GetInteractionListErr       = business_error.NewBusinessError("get interaction list failed", -10007)
)

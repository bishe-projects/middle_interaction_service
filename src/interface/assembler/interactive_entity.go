package assembler

import (
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_entity/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

func ConvertCreateReqToEntity(req *interaction.CreateEntityReq) *entity.InteractiveEntity {
	return &entity.InteractiveEntity{
		BizId: req.BizId,
		Name:  req.Name,
		Desc:  req.Desc,
	}
}

func ConvertEntityToInteractiveEntity(entity *entity.InteractiveEntity) *interaction.InteractiveEntity {
	return &interaction.InteractiveEntity{
		Id:    entity.ID,
		BizId: entity.BizId,
		Name:  entity.Name,
		Desc:  entity.Desc,
	}
}

func ConvertEntityListToInteractiveEntityList(entityList []*entity.InteractiveEntity) []*interaction.InteractiveEntity {
	interactiveEntityList := make([]*interaction.InteractiveEntity, 0, len(entityList))
	for _, e := range entityList {
		interactiveEntityList = append(interactiveEntityList, ConvertEntityToInteractiveEntity(e))
	}
	return interactiveEntityList
}

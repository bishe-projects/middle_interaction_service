package assembler

import (
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interactive_action/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

func ConvertCreateReqToAction(req *interaction.CreateActionReq) *entity.InteractiveAction {
	return &entity.InteractiveAction{
		BizId: req.BizId,
		Name:  req.Name,
		Desc:  req.Desc,
	}
}

func ConvertActionToInteractiveAction(action *entity.InteractiveAction) *interaction.InteractiveAction {
	return &interaction.InteractiveAction{
		Id:    action.ID,
		BizId: action.BizId,
		Name:  action.Name,
		Desc:  action.Desc,
	}
}

func ConvertActionListToInteractiveActionList(actionList []*entity.InteractiveAction) []*interaction.InteractiveAction {
	interactiveActionList := make([]*interaction.InteractiveAction, 0, len(actionList))
	for _, a := range actionList {
		interactiveActionList = append(interactiveActionList, ConvertActionToInteractiveAction(a))
	}
	return interactiveActionList
}

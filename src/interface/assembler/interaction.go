package assembler

import (
	"gitlab.com/bishe-projects/middle_interaction_service/src/domain/interaction/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

func ConvertInteractionToInteractionEntity(interaction *interaction.Interaction) *entity.Interaction {
	return &entity.Interaction{
		BizId:             interaction.BizId,
		ActionId:          interaction.ActionId,
		InitiatorEntityId: interaction.InitiatorEntityId,
		InitiatorId:       interaction.InitiatorId,
		AcceptorEntityId:  interaction.AcceptorEntityId,
		AcceptorId:        interaction.AcceptorId,
	}
}

func ConvertInteractionEntityToInteraction(interactionEntity *entity.Interaction) *interaction.Interaction {
	return &interaction.Interaction{
		BizId:             interactionEntity.BizId,
		ActionId:          interactionEntity.ActionId,
		InitiatorEntityId: interactionEntity.InitiatorEntityId,
		InitiatorId:       interactionEntity.InitiatorId,
		AcceptorEntityId:  interactionEntity.AcceptorEntityId,
		AcceptorId:        interactionEntity.AcceptorId,
	}
}

func ConvertInteractionEntityListToInteractionList(interactionEntityList []*entity.Interaction) []*interaction.Interaction {
	interactionList := make([]*interaction.Interaction, 0, len(interactionEntityList))
	for _, interactionEntity := range interactionEntityList {
		interactionList = append(interactionList, ConvertInteractionEntityToInteraction(interactionEntity))
	}
	return interactionList
}

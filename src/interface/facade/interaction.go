package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/application/service"
	"gitlab.com/bishe-projects/middle_interaction_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

var InteractionFacade = new(Interaction)

type Interaction struct{}

func (f *Interaction) Create(ctx context.Context, req *interaction.CreateInteractionReq) *interaction.CreateInteractionResp {
	resp := interaction.NewCreateInteractionResp()
	err := service.InteractionApp.Create(ctx, assembler.ConvertInteractionToInteractionEntity(req.Interaction))
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractionFacade] create interaction failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *Interaction) Del(ctx context.Context, req *interaction.RemoveInteractionReq) *interaction.RemoveInteractionResp {
	resp := interaction.NewRemoveInteractionResp()
	err := service.InteractionApp.Del(ctx, assembler.ConvertInteractionToInteractionEntity(req.Interaction))
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractionFacade] delete interaction failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
	}
	return resp
}

func (f *Interaction) GetList(ctx context.Context, req *interaction.GetInteractionListReq) *interaction.GetInteractionListResp {
	resp := interaction.NewGetInteractionListResp()
	interactionEntityList, err := service.InteractionApp.GetList(ctx, req.BizId, req.ActionId, req.InitiatorEntityId, req.AcceptorEntityId, req.InitiatorId, req.AcceptorId)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractionFacade] get interaction list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.InteractionList = assembler.ConvertInteractionEntityListToInteractionList(interactionEntityList)
	return resp
}

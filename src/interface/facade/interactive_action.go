package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/application/service"
	"gitlab.com/bishe-projects/middle_interaction_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

var InteractiveActionFacade = new(InteractiveAction)

type InteractiveAction struct{}

func (f *InteractiveAction) Create(ctx context.Context, req *interaction.CreateActionReq) *interaction.CreateActionResp {
	resp := interaction.NewCreateActionResp()
	action := assembler.ConvertCreateReqToAction(req)
	err := service.InteractiveActionApp.Create(action)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractiveActionFacade] create interactive action failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Action = assembler.ConvertActionToInteractiveAction(action)
	return resp
}

func (f *InteractiveAction) All(ctx context.Context, req *interaction.AllActionListReq) *interaction.AllActionListResp {
	resp := interaction.NewAllActionListResp()
	actionList, err := service.InteractiveActionApp.All(req.BizId)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractiveActionFacade] get all interactive action list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.ActionList = assembler.ConvertActionListToInteractiveActionList(actionList)
	return resp
}

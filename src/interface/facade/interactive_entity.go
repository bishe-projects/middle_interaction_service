package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/middle_interaction_service/src/application/service"
	"gitlab.com/bishe-projects/middle_interaction_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

var InteractiveEntityFacade = new(InteractiveEntity)

type InteractiveEntity struct{}

func (f *InteractiveEntity) Create(ctx context.Context, req *interaction.CreateEntityReq) *interaction.CreateEntityResp {
	resp := interaction.NewCreateEntityResp()
	entity := assembler.ConvertCreateReqToEntity(req)
	err := service.InteractiveEntityApp.Create(entity)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractiveEntityFacade] create interactive entity failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Entity = assembler.ConvertEntityToInteractiveEntity(entity)
	return resp
}

func (f *InteractiveEntity) All(ctx context.Context, req *interaction.AllEntityListReq) *interaction.AllEntityListResp {
	resp := interaction.NewAllEntityListResp()
	entityList, err := service.InteractiveEntityApp.All(req.BizId)
	if err != nil {
		klog.CtxErrorf(ctx, "[InteractiveEntityFacade] get all interactive entity list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.EntityList = assembler.ConvertEntityListToInteractiveEntityList(entityList)
	return resp
}

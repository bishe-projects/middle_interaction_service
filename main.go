package main

import (
	"github.com/cloudwego/kitex/server"
	interaction "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction/interactionservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8888")
	svr := interaction.NewServer(new(InteractionServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}

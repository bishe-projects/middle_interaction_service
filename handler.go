package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/middle_interaction_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

// InteractionServiceImpl implements the last service interface defined in the IDL.
type InteractionServiceImpl struct{}

// CreateEntity implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) CreateEntity(ctx context.Context, req *interaction.CreateEntityReq) (resp *interaction.CreateEntityResp, err error) {
	klog.CtxInfof(ctx, "CreateEntity req=%+v", req)
	resp = facade.InteractiveEntityFacade.Create(ctx, req)
	klog.CtxInfof(ctx, "CreateEntity resp=%+v", resp)
	return
}

// AllEntityList implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) AllEntityList(ctx context.Context, req *interaction.AllEntityListReq) (resp *interaction.AllEntityListResp, err error) {
	klog.CtxInfof(ctx, "AllEntityList req=%+v", req)
	resp = facade.InteractiveEntityFacade.All(ctx, req)
	klog.CtxInfof(ctx, "AllEntityList resp=%+v", resp)
	return
}

// CreateAction implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) CreateAction(ctx context.Context, req *interaction.CreateActionReq) (resp *interaction.CreateActionResp, err error) {
	klog.CtxInfof(ctx, "CreateAction req=%+v", req)
	resp = facade.InteractiveActionFacade.Create(ctx, req)
	klog.CtxInfof(ctx, "CreateAction resp=%+v", resp)
	return
}

// AllActionList implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) AllActionList(ctx context.Context, req *interaction.AllActionListReq) (resp *interaction.AllActionListResp, err error) {
	klog.CtxInfof(ctx, "AllActionList req=%+v", req)
	resp = facade.InteractiveActionFacade.All(ctx, req)
	klog.CtxInfof(ctx, "AllActionList resp=%+v", resp)
	return
}

// CreateInteraction implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) CreateInteraction(ctx context.Context, req *interaction.CreateInteractionReq) (resp *interaction.CreateInteractionResp, err error) {
	klog.CtxInfof(ctx, "CreateInteraction req=%+v", req)
	resp = facade.InteractionFacade.Create(ctx, req)
	klog.CtxInfof(ctx, "CreateInteraction resp=%+v", resp)
	return
}

// RemoveInteraction implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) RemoveInteraction(ctx context.Context, req *interaction.RemoveInteractionReq) (resp *interaction.RemoveInteractionResp, err error) {
	klog.CtxInfof(ctx, "RemoveInteraction req=%+v", req)
	resp = facade.InteractionFacade.Del(ctx, req)
	klog.CtxInfof(ctx, "RemoveInteraction resp=%+v", resp)
	return
}

// GetInteractionList implements the InteractionServiceImpl interface.
func (s *InteractionServiceImpl) GetInteractionList(ctx context.Context, req *interaction.GetInteractionListReq) (resp *interaction.GetInteractionListResp, err error) {
	klog.CtxInfof(ctx, "GetInteractionList req=%+v", req)
	resp = facade.InteractionFacade.GetList(ctx, req)
	klog.CtxInfof(ctx, "GetInteractionList resp=%+v", resp)
	return
}
